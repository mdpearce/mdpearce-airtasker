package com.example.airfeeder.io;

import android.support.annotation.Nullable;
import android.util.SparseArray;

import com.example.airfeeder.io.remote.AirFeederService;
import com.example.airfeeder.model.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskRepository {
    private static TaskRepository ourInstance;
    private static final Object syncLock = new Object();

    private final AirFeederService remoteService;

    private SparseArray<Task> cachedTasks = new SparseArray<>();

    private SparseArray<Call<Task>> taskCalls = new SparseArray<>();

    private SparseArray<List<DataCallback<Task>>> pendingCallbacks = new SparseArray<>();

    public static TaskRepository getInstance(AirFeederService remoteService) {
        if (ourInstance == null) {
            synchronized (syncLock) {
                if (ourInstance == null) {
                    ourInstance = new TaskRepository(remoteService);
                }
            }
        }
        return ourInstance;
    }

    private TaskRepository(AirFeederService remoteService) {
        this.remoteService = remoteService;
    }

    public void getTask(int taskId, DataCallback<Task> callback) {
        Task cachedTask = cachedTasks.get(taskId);
        if (cachedTask != null) {
            callback.onSuccess(cachedTask);
        } else {
            performRemoteFetchAndCache(taskId, callback);
        }
    }

    private void performRemoteFetchAndCache(final int taskId, final DataCallback<Task> callback) {
        if (taskCalls.get(taskId) == null) {
            Call<Task> call = remoteService.task(taskId);
            taskCalls.put(taskId, call);

            call.enqueue(new Callback<Task>() {
                @Override
                public void onResponse(Call<Task> call, Response<Task> response) {
                    Task taskResponse = response.body();
                    if (response.isSuccessful() && taskResponse != null) {
                        cachedTasks.put(taskId, taskResponse);
                        callback.onSuccess(taskResponse);
                    } else {
                        callback.onError("Unable to perform remote fetch of task data");
                    }
                    firePendingCallbacks(taskId, null);
                    taskCalls.remove(taskId);
                }

                @Override
                public void onFailure(Call<Task> call, Throwable error) {
                    String errorMessage = "Unable to perform remote fetch of task data: " + error.getLocalizedMessage();
                    callback.onError(errorMessage);
                    firePendingCallbacks(taskId, errorMessage);
                    taskCalls.remove(taskId);
                }
            });
        } else {
            addPendingCallback(taskId, callback);
        }
    }

    private void addPendingCallback(int taskId, DataCallback<Task> callback) {
        List<DataCallback<Task>> callbacks = pendingCallbacks.get(taskId);
        if (callbacks == null) {
            callbacks = new ArrayList<>();
            pendingCallbacks.put(taskId, callbacks);
        }
        callbacks.add(callback);
    }

    private void firePendingCallbacks(int taskId, @Nullable String error) {
        List<DataCallback<Task>> callbacks = pendingCallbacks.get(taskId);
        if (callbacks != null) {
            Iterator<DataCallback<Task>> iterator = callbacks.iterator();
            while (iterator.hasNext()) {
                DataCallback<Task> callback = iterator.next();
                if (error != null) {
                    callback.onError(error);
                } else {
                    callback.onSuccess(cachedTasks.get(taskId));
                }
                iterator.remove();
            }
        }
    }
}
