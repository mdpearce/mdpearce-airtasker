package com.example.airfeeder.io.remote;

public class ImageUrlFormatter {

    private static final String BASE_URL = "https://stage.airtasker.com/android-code-test/";

    public static String formatAvatarMiniUrl(String avatarMiniUrl) {
        if (avatarMiniUrl == null || avatarMiniUrl.isEmpty()) {
            return null;
        } else {
            return BASE_URL + avatarMiniUrl;
        }
    }
}
