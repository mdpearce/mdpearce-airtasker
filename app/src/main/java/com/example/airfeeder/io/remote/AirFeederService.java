package com.example.airfeeder.io.remote;

import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.RawFeedEntry;
import com.example.airfeeder.model.Task;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface AirFeederService {
    @GET("feed.json")
    Call<List<RawFeedEntry>> feedEntries();

    @GET("task/{id}.json")
    Call<Task> task(@Path("id") int taskId);

    @GET("profile/{id}.json")
    Call<Profile> profile(@Path("id") int profileId);
}
