package com.example.airfeeder.io;

import com.example.airfeeder.model.FeedEntry;

import java.util.List;

public interface AirFeederProvider {
    void getFeedEntries();

    void addFeedUpdateListener(FeedUpdateListener feedUpdateListener);

    void removeFeedUpdateListener(FeedUpdateListener feedUpdateListener);

    void getFeedEntries(boolean forceRefresh);

    interface FeedUpdateListener {
        void feedUpdated(List<FeedEntry> feedEntries);
    }
}
