package com.example.airfeeder.io;

import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import com.example.airfeeder.io.remote.AirFeederService;
import com.example.airfeeder.model.Profile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileRepository {
    private static ProfileRepository ourInstance;
    private static final Object syncLock = new Object();

    private final AirFeederService remoteService;

    private SparseArray<Profile> cachedProfiles = new SparseArray<>();

    private SparseArray<Call<Profile>> profileCalls = new SparseArray<>();

    private SparseArray<List<DataCallback<Profile>>> pendingCallbacks = new SparseArray<>();

    public static ProfileRepository getInstance(AirFeederService remoteService) {
        if (ourInstance == null) {
            synchronized (syncLock) {
                if (ourInstance == null) {
                    ourInstance = new ProfileRepository(remoteService);
                }
            }
        }
        return ourInstance;
    }

    private ProfileRepository(AirFeederService remoteService) {
        this.remoteService = remoteService;
    }

    public void getProfile(int profileId, DataCallback<Profile> callback) {
        Profile cachedProfile = cachedProfiles.get(profileId);
        if (cachedProfile != null) {
            callback.onSuccess(cachedProfile);
        } else {
            performRemoteFetchAndCache(profileId, callback);
        }
    }

    private void performRemoteFetchAndCache(final int profileId, final DataCallback<Profile> callback) {
        if (profileCalls.get(profileId) == null) {

            Call<Profile> call = remoteService.profile(profileId);
            profileCalls.put(profileId, call);

            call.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {
                    Profile profileResponse = response.body();
                    if (response.isSuccessful() && profileResponse != null) {
                        cachedProfiles.put(profileId, profileResponse);
                        callback.onSuccess(profileResponse);
                    } else {
                        callback.onError("Unable to perform remote fetch of profile data");
                    }
                    firePendingCallbacks(profileId, null);
                    profileCalls.remove(profileId);
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable error) {
                    String errorMessage = "Unable to perform remote fetch of profile data: " + error.getLocalizedMessage();
                    callback.onError(errorMessage);
                    firePendingCallbacks(profileId, errorMessage);
                    profileCalls.remove(profileId);
                }
            });
        } else {
            addPendingCallback(profileId, callback);
        }
    }

    private void addPendingCallback(int profileId, DataCallback<Profile> callback) {
        List<DataCallback<Profile>> callbacks = pendingCallbacks.get(profileId);
        if (callbacks == null) {
            callbacks = new ArrayList<>();
            pendingCallbacks.put(profileId, callbacks);
        }
        callbacks.add(callback);
    }

    private void firePendingCallbacks(int profileId, @Nullable String error) {
        List<DataCallback<Profile>> callbacks = pendingCallbacks.get(profileId);
        if (callbacks != null) {
            Iterator<DataCallback<Profile>> iterator = callbacks.iterator();
            while (iterator.hasNext()) {
                DataCallback<Profile> callback = iterator.next();
                if (error != null) {
                    callback.onError(error);
                } else {
                    callback.onSuccess(cachedProfiles.get(profileId));
                }
                iterator.remove();
            }
        }
    }
}
