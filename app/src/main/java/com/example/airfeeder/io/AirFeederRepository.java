package com.example.airfeeder.io;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.airfeeder.io.remote.AirFeederService;
import com.example.airfeeder.io.remote.AirFeederServiceFactory;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.RawFeedEntry;
import com.example.airfeeder.model.Task;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AirFeederRepository implements AirFeederProvider {

    private static AirFeederRepository ourInstance;
    private static final Object syncLock = new Object();

    private final AirFeederService remoteService;
    private final ProfileRepository profileRepository;
    private final TaskRepository taskRepository;

    private List<FeedEntry> cachedFeed;

    private AtomicInteger repositoryCallsInProgress = new AtomicInteger(0);

    private Set<FeedUpdateListener> feedUpdateListeners = new HashSet<>();

    private static final String TAG = "AirFeederRepo";

    public static AirFeederRepository getInstance() {
        if (ourInstance == null) {
            synchronized (syncLock) {
                if (ourInstance == null) {
                    AirFeederService remoteService = new AirFeederServiceFactory().getService();
                    ourInstance = new AirFeederRepository(remoteService, ProfileRepository.getInstance(remoteService), TaskRepository.getInstance(remoteService), new ArrayList<FeedEntry>());
                }
            }
        }
        return ourInstance;
    }

    AirFeederRepository(AirFeederService remoteService, ProfileRepository profileRepository, TaskRepository taskRepository, List<FeedEntry> localCache) {
        this.remoteService = remoteService;
        this.profileRepository = profileRepository;
        this.taskRepository = taskRepository;
        this.cachedFeed = localCache;
    }

    @Override
    public void getFeedEntries() {
        getFeedEntries(false);
    }

    @Override
    public void getFeedEntries(boolean forceRefresh) {

        if (repositoryCallsAreInProgress()) {
            Log.d(TAG, "Feed is already being fetched, listeners will be updated when complete");
            return;
        }

        if (forceRefresh) {
            cachedFeed.clear();
        }

        if (!cachedFeedIsDirty()) {
            updateFeedUpdateListeners(cachedFeed);
        } else {
            performRemoteFetchAndCache();
        }
    }

    private boolean repositoryCallsAreInProgress() {
        return repositoryCallsInProgress.get() > 0;
    }

    private void performRemoteFetchAndCache() {
        incrementRepositoryCalls();
        remoteService.feedEntries().enqueue(new Callback<List<RawFeedEntry>>() {
            @Override
            public void onResponse(@NonNull Call<List<RawFeedEntry>> call, @NonNull Response<List<RawFeedEntry>> response) {
                List<RawFeedEntry> rawFeedEntries = response.body();
                if (response.isSuccessful() && rawFeedEntries != null && rawFeedEntries.size() > 0) {
                    mapRawFeedEntries(rawFeedEntries);
                    decrementRepositoryCalls();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<RawFeedEntry>> call, @NonNull Throwable t) {
                decrementRepositoryCalls();
            }
        });
    }

    private void decrementRepositoryCalls() {
        int callsInProgress = repositoryCallsInProgress.decrementAndGet();
        if (callsInProgress <= 0) {
            updateFeedUpdateListeners(cachedFeed);
        }
    }

    private void incrementRepositoryCalls() {
        repositoryCallsInProgress.incrementAndGet();
    }

    private void mapRawFeedEntries(final List<RawFeedEntry> rawFeedEntries) {
        for (int i = 0; i < rawFeedEntries.size(); i++) {

            RawFeedEntry rawFeedEntry = rawFeedEntries.get(i);

            final FeedEntry feedEntry = new FeedEntry(rawFeedEntry);
            cachedFeed.add(feedEntry);

            incrementRepositoryCalls();
            taskRepository.getTask(rawFeedEntry.getTaskId(), new DataCallback<Task>() {
                @Override
                public void onSuccess(Task data) {
                    feedEntry.setTask(data);
                    decrementRepositoryCalls();
                }

                @Override
                public void onError(String message) {
                    decrementRepositoryCalls();
                }
            });

            incrementRepositoryCalls();
            profileRepository.getProfile(rawFeedEntry.getProfileId(), new DataCallback<Profile>() {
                @Override
                public void onSuccess(Profile data) {
                    feedEntry.setProfile(data);
                    decrementRepositoryCalls();
                }

                @Override
                public void onError(String message) {
                    decrementRepositoryCalls();
                }
            });
        }
    }

    private boolean cachedFeedIsDirty() {
        return cachedFeed == null || cachedFeed.size() == 0;
    }

    @Override
    public void addFeedUpdateListener(FeedUpdateListener feedUpdateListener) {
        feedUpdateListeners.add(feedUpdateListener);
    }

    @Override
    public void removeFeedUpdateListener(FeedUpdateListener feedUpdateListener) {
        feedUpdateListeners.remove(feedUpdateListener);
    }

    private void updateFeedUpdateListeners(List<FeedEntry> feedEntries) {
        for (FeedUpdateListener feedUpdateListener : feedUpdateListeners) {
            feedUpdateListener.feedUpdated(feedEntries);
        }
    }
}
