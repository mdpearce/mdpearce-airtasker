package com.example.airfeeder.io;

public interface DataCallback<T> {
    void onSuccess(T data);

    void onError(String message);
}
