package com.example.airfeeder.io.remote;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AirFeederServiceFactory {

    private static final String BASE_URL = "https://stage.airtasker.com/android-code-test/";
    private static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    public AirFeederService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(getGsonConverterFactory())
                .build();

        return retrofit.create(AirFeederService.class);
    }

    private GsonConverterFactory getGsonConverterFactory() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat(ISO_8601_DATE_FORMAT);
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
