package com.example.airfeeder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.airfeeder.views.FeedListView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedActivity extends AppCompatActivity {

    @BindView(R.id.feed_list_view)
    FeedListView feedListView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(R.string.activity_name);

        feedListView.refreshData();
    }

    @Override
    protected void onDestroy() {
        feedListView.destroy();
        super.onDestroy();
    }
}
