package com.example.airfeeder.model;

public class Task {
    private int id;
    private String name;
    private String description;
    private TaskState state;
    private int posterId;
    private int workerId;

    public Task(int id, String name, String description, TaskState state, int posterId, int workerId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.state = state;
        this.posterId = posterId;
        this.workerId = workerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    public int getPosterId() {
        return posterId;
    }

    public void setPosterId(int posterId) {
        this.posterId = posterId;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }
}
