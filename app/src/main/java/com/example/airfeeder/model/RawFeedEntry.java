package com.example.airfeeder.model;

import java.util.Date;

public class RawFeedEntry {
    private int taskId;
    private int profileId;
    private String text;
    private Date createdAt;
    private Event event;

    public RawFeedEntry(int taskId, int profileId, String text, Date createdAt, Event event) {
        this.taskId = taskId;
        this.profileId = profileId;
        this.text = text;
        this.createdAt = createdAt;
        this.event = event;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
