package com.example.airfeeder.model;

import java.util.Date;

public class FeedEntry {
    private Task task;
    private Profile profile;
    private String text;
    private Date createdAt;
    private Event event;

    public FeedEntry(Task task, Profile profile, String text, Date createdAt, Event event) {
        this.task = task;
        this.profile = profile;
        this.text = text;
        this.createdAt = createdAt;
        this.event = event;
    }

    public FeedEntry(RawFeedEntry entry) {
        this.text = entry.getText();
        this.createdAt = entry.getCreatedAt();
        this.event = entry.getEvent();
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Task getTask() {
        return task;
    }

    public Profile getProfile() {
        return profile;
    }

    public String getText() {
        return text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Event getEvent() {
        return event;
    }
}
