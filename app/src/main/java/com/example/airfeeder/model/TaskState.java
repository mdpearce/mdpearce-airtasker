package com.example.airfeeder.model;

import com.google.gson.annotations.SerializedName;

public enum TaskState {
    @SerializedName("assigned")ASSIGNED,
    @SerializedName("posted")POSTED
}
