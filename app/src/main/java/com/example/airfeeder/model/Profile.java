package com.example.airfeeder.model;

public class Profile {
    private int id;
    private String avatarMiniUrl;
    private String firstName;
    private int rating; // Based on viewing the available data, int is correct here, but often
                        // profile ratings are defined as floats

    public Profile(int id, String avatarMiniUrl, String firstName, int rating) {
        this.id = id;
        this.avatarMiniUrl = avatarMiniUrl;
        this.firstName = firstName;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarMiniUrl() {
        return avatarMiniUrl;
    }

    public void setAvatarMiniUrl(String avatarMiniUrl) {
        this.avatarMiniUrl = avatarMiniUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
