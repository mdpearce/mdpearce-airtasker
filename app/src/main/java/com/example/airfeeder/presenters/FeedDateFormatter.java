package com.example.airfeeder.presenters;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.Locale;

public class FeedDateFormatter {
    private static final String FEED_ENTRY_DATE_FORMAT = "%1$ta %1$tl:%1$tM%1$tp";

    private final Locale locale;

    public FeedDateFormatter(Locale locale) {
        this.locale = locale;
    }

    public String formatDate(@NonNull Date date) {
        return String.format(locale, FEED_ENTRY_DATE_FORMAT, date);
    }
}
