package com.example.airfeeder.presenters;

import android.support.v4.widget.SwipeRefreshLayout;

import com.example.airfeeder.adapters.FeedAdapter;
import com.example.airfeeder.io.AirFeederProvider;
import com.example.airfeeder.io.AirFeederRepository;
import com.example.airfeeder.model.FeedEntry;

import java.util.List;

public class FeedListPresenter implements AirFeederRepository.FeedUpdateListener, SwipeRefreshLayout.OnRefreshListener {

    private final View view;
    private final FeedAdapter adapter;
    private final AirFeederProvider repository;

    public FeedListPresenter(View view) {
        this(view, new FeedAdapter(), AirFeederRepository.getInstance());
    }

    FeedListPresenter(View view, FeedAdapter adapter, AirFeederProvider repository) {
        this.view = view;
        this.adapter = adapter;
        this.repository = repository;
        view.setAdapter(adapter);
        repository.addFeedUpdateListener(this);
        view.setOnRefreshListener(this);
    }

    public void refreshData() {
        if (!view.isRefreshing()) {
            view.setRefreshing();
        }

        repository.getFeedEntries();
    }

    @Override
    public void feedUpdated(List<FeedEntry> feedEntries) {
        adapter.setData(feedEntries);
        view.setNotRefreshing();
    }

    public void destroy() {
        repository.removeFeedUpdateListener(this);
    }

    @Override
    public void onRefresh() {
        repository.getFeedEntries(true);
    }

    public interface View {
        void setAdapter(FeedAdapter adapter);

        void setRefreshing();

        void setNotRefreshing();

        void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener);

        boolean isRefreshing();
    }
}
