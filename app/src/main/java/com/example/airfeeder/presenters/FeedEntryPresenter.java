package com.example.airfeeder.presenters;

import com.example.airfeeder.R;
import com.example.airfeeder.io.remote.ImageUrlFormatter;
import com.example.airfeeder.model.Event;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.Task;

import java.util.Date;
import java.util.Locale;

public class FeedEntryPresenter {

    private final View view;
    private final FeedDateFormatter feedDateFormatter;

    public FeedEntryPresenter(View view) {
        this(view, new FeedDateFormatter(Locale.getDefault()));
    }

    FeedEntryPresenter(View view, FeedDateFormatter feedDateFormatter) {
        this.view = view;
        this.feedDateFormatter = feedDateFormatter;
    }

    public void setData(final FeedEntry entry) {
        clearFields();

        if (entry == null) {
            return;
        }
        final Profile profile = entry.getProfile();
        final Task task = entry.getTask();
        final Event event = entry.getEvent();
        final Date createdAt = entry.getCreatedAt();

        if (profile != null && task != null) {
            view.setDescriptionText(new DescriptionFormatter(profile, task).formatDescription(entry.getText()));
        }

        if (createdAt != null) {
            view.setDateText(feedDateFormatter.formatDate(createdAt));
        }

        if (event != null) {
            view.setEventText(event.name().toLowerCase(Locale.getDefault()));
        }

        if (profile != null) {
            view.setAvatarUrl(ImageUrlFormatter.formatAvatarMiniUrl(entry.getProfile().getAvatarMiniUrl()), R.drawable.ic_person_black_96dp);
        }
    }

    private void clearFields() {
        view.setDescriptionText("");
        view.setDateText("");
        view.setEventText("");
        view.setAvatarDrawable(R.drawable.ic_person_black_96dp);
    }

    public interface View {

        void setDescriptionText(final String text);

        void setDateText(final String text);

        void setEventText(final String text);

        void setAvatarUrl(final String url, final int placeholderDrawableRes);

        void setAvatarDrawable(final int drawableResource);
    }
}
