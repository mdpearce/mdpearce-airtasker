package com.example.airfeeder.presenters;

import android.support.annotation.NonNull;

import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.Task;

import java.util.regex.Pattern;

public class DescriptionFormatter {
    private final Profile profile;
    private final Task task;

    private static final Pattern profileNamePattern;
    private static final Pattern taskNamePattern;

    static {
        profileNamePattern = Pattern.compile("\\{profileName\\}");
        taskNamePattern = Pattern.compile("\\{taskName\\}");
    }


    public DescriptionFormatter(@NonNull Profile profile, @NonNull Task task) {
        this.profile = profile;
        this.task = task;
    }

    public String formatDescription(@NonNull final String description) {
        String formattedDescription = profileNamePattern
                .matcher(description)
                .replaceAll(profile.getFirstName());

        formattedDescription = taskNamePattern
                .matcher(formattedDescription)
                .replaceAll("\"" + task.getName() + "\"");

        return formattedDescription;
    }

}
