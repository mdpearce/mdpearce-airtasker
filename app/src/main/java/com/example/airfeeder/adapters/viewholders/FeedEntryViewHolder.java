package com.example.airfeeder.adapters.viewholders;

import android.support.v7.widget.RecyclerView;

import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.views.FeedEntryView;


public class FeedEntryViewHolder extends RecyclerView.ViewHolder {

    public FeedEntryViewHolder(FeedEntryView itemView) {
        super(itemView);
    }

    public void setData(FeedEntry entry) {
        ((FeedEntryView) itemView).setData(entry);
    }
}
