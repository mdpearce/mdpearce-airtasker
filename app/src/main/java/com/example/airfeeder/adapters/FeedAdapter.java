package com.example.airfeeder.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.airfeeder.adapters.viewholders.FeedEntryViewHolder;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.views.FeedEntryView;

import java.util.List;


public class FeedAdapter extends RecyclerView.Adapter<FeedEntryViewHolder> {

    private List<FeedEntry> feedEntries;

    @Override
    public FeedEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FeedEntryView view = new FeedEntryView(parent.getContext());
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(params);
        return new FeedEntryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedEntryViewHolder holder, int position) {
        holder.setData(feedEntries.get(position));
    }

    @Override
    public int getItemCount() {
        if (feedEntries == null) {
            return 0;
        } else {
            return feedEntries.size();
        }
    }

    public void setData(List<FeedEntry> feedEntries) {
        if (this.feedEntries != null) {
            notifyItemRangeRemoved(0, this.feedEntries.size());
        }

        this.feedEntries = feedEntries;

        notifyItemRangeInserted(0, feedEntries.size());
    }
}
