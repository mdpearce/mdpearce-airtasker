package com.example.airfeeder.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.airfeeder.R;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.presenters.FeedEntryPresenter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedEntryView extends ConstraintLayout implements FeedEntryPresenter.View {

    private FeedEntryPresenter presenter;

    @BindView(R.id.image_avatar)
    ImageView imgAvatar;
    @BindView(R.id.text_description)
    TextView textDescription;
    @BindView(R.id.text_date)
    TextView textDate;
    @BindView(R.id.text_event)
    TextView textEvent;

    public FeedEntryView(Context context) {
        super(context);
        init();
    }

    public FeedEntryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FeedEntryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_feed_entry, this, true);
        ButterKnife.bind(this);
        presenter = new FeedEntryPresenter(this);
    }

    public void setData(final FeedEntry entry) {
        presenter.setData(entry);
    }

    @Override
    public void setDescriptionText(final String text) {
        textDescription.setText(text);
    }

    @Override
    public void setDateText(final String text) {
        textDate.setText(text);
    }

    @Override
    public void setEventText(final String text) {
        textEvent.setText(text);
    }

    @Override
    public void setAvatarUrl(final String url, final int placeholderDrawableRes) {
        Picasso.with(getContext())
                .load(url)
                .placeholder(placeholderDrawableRes)
                .resizeDimen(R.dimen.avatar_size, R.dimen.avatar_size)
                .centerCrop()
                .into(imgAvatar);
    }

    @Override
    public void setAvatarDrawable(final int drawableResource) {
        imgAvatar.setImageResource(drawableResource);
    }
}
