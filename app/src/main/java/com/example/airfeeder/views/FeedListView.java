package com.example.airfeeder.views;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.example.airfeeder.R;
import com.example.airfeeder.adapters.FeedAdapter;
import com.example.airfeeder.presenters.FeedListPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedListView extends SwipeRefreshLayout implements FeedListPresenter.View {

    private FeedListPresenter presenter;

    @BindView(R.id.feed_recycler_view)
    RecyclerView recyclerView;

    public FeedListView(Context context) {
        super(context);
        init();
    }

    public FeedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void destroy() {
        presenter.destroy();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_feed_list, this, true);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        presenter = new FeedListPresenter(this);
    }

    @Override
    public void setAdapter(FeedAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setRefreshing() {
        setRefreshing(true);
    }

    @Override
    public void setNotRefreshing() {
        setRefreshing(false);
    }

    public void refreshData() {
        presenter.refreshData();
    }
}
