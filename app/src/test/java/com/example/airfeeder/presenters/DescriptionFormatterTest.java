package com.example.airfeeder.presenters;

import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.Task;
import com.example.airfeeder.model.TaskState;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DescriptionFormatterTest {
    private DescriptionFormatter descriptionFormatter;

    private static final String TEST_NAME = "Michael";
    private static final String TEST_TASK_NAME = "Some task";

    private static final Profile TEST_PROFILE = new Profile(1, "/someurl.jpg", TEST_NAME, 4);
    private static final Task TEST_TASK = new Task(2, TEST_TASK_NAME, "Some description", TaskState.ASSIGNED, 1, 2);

    private String rawDescription;
    private String formattedDescription;

    @Before
    public void setUp() throws Exception {
        descriptionFormatter = new DescriptionFormatter(TEST_PROFILE, TEST_TASK);
    }

    @Test
    public void shouldFormatDescriptionCorrectly() throws Exception {
        givenRawDescriptionIs("{profileName} asked a question about {taskName}");
        whenFormatterCalled();
        thenFormattedDescriptionShouldBe(TEST_NAME + " asked a question about \"" + TEST_TASK_NAME + "\"");
    }

    @Test
    public void shouldNotReplaceMalformedProfileToken() throws Exception {
        givenRawDescriptionIs("{profile} asked a question about {taskName}");
        whenFormatterCalled();
        thenFormattedDescriptionShouldBe("{profile} asked a question about \"" + TEST_TASK_NAME + "\"");
    }

    @Test
    public void shouldNotReplaceMalformedTaskToken() throws Exception {
        givenRawDescriptionIs("{profileName} asked a question about {task}");
        whenFormatterCalled();
        thenFormattedDescriptionShouldBe(TEST_NAME + " asked a question about {task}");
    }

    @Test
    public void shouldNotReplaceTokensWithoutBraces() throws Exception {
        givenRawDescriptionIs("profileName asked a question about taskName");
        whenFormatterCalled();
        thenFormattedDescriptionShouldBe("profileName asked a question about taskName");
    }

    private void thenFormattedDescriptionShouldBe(String formattedDescription) {
        assertThat(this.formattedDescription, is(formattedDescription));
    }

    private void whenFormatterCalled() {
        formattedDescription = descriptionFormatter.formatDescription(rawDescription);
    }

    private void givenRawDescriptionIs(String description) {
        rawDescription = description;
    }

}