package com.example.airfeeder.presenters;

import com.example.airfeeder.adapters.FeedAdapter;
import com.example.airfeeder.io.AirFeederProvider;
import com.example.airfeeder.model.Event;
import com.example.airfeeder.model.FeedEntry;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FeedListPresenterTest {

    private FeedListPresenter feedListPresenter;

    @Mock
    private FeedListPresenter.View mockView;

    @Mock
    private FeedAdapter mockAdapter;

    @Mock
    private AirFeederProvider mockRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        feedListPresenter = new FeedListPresenter(mockView, mockAdapter, mockRepository);
    }

    @Test
    public void shouldSetRefreshingIfNotAlreadyRefreshingAndRefreshCalled() throws Exception {
        givenViewIsNotRefreshing();
        whenRefreshDataCalled();
        thenViewShouldSetRefreshing();
    }

    @Test
    public void shouldNotSetRefreshingIfAlreadyRefreshingAndRefreshCalled() throws Exception {
        givenViewIsRefreshing();
        whenRefreshDataCalled();
        thenViewShouldNotSetRefreshing();
    }

    @Test
    public void shouldRequestForceUpdateOfFeedEntriesWhenRefreshCalled() throws Exception {
        whenRefreshDataCalled();
        thenRepositoryShouldRefresh();
    }

    @Test
    public void shouldSetDataOnAdapterWhenRepositoryUpdates() throws Exception {
        List<FeedEntry> feedEntries = getListOfFeedEntries(3);
        whenFeedUpdatedWith(feedEntries);
        thenAdapterShouldSetData(feedEntries);
    }

    @Test
    public void shouldSetNotRefreshingWhenRepositoryUpdates() throws Exception {
        whenFeedUpdatedWith(getListOfFeedEntries(3));
        thenViewShouldSetNotRefreshing();
    }

    @Test
    public void shouldRemoveFeedUpdateListenerOnDestroy() throws Exception {
        whenDestroyCalled();
        thenRepositoryShouldRemoveFeedUpdateListener(feedListPresenter);
    }

    @Test
    public void shouldRefreshDataWhenSwipeRefreshed() throws Exception {
        whenSwipeRefreshed();
        thenRepositoryShouldForceRefresh();
    }

    private void whenSwipeRefreshed() {
        feedListPresenter.onRefresh();
    }

    private void thenRepositoryShouldRemoveFeedUpdateListener(AirFeederProvider.FeedUpdateListener listener) {
        verify(mockRepository).removeFeedUpdateListener(listener);
    }

    private void whenDestroyCalled() {
        feedListPresenter.destroy();
    }

    private void thenViewShouldSetNotRefreshing() {
        verify(mockView).setNotRefreshing();
    }

    private void thenAdapterShouldSetData(List<FeedEntry> feedEntries) {
        verify(mockAdapter).setData(feedEntries);
    }

    private void whenFeedUpdatedWith(List<FeedEntry> feedEntries) {
        feedListPresenter.feedUpdated(feedEntries);
    }

    private void thenRepositoryShouldForceRefresh() {
        verify(mockRepository).getFeedEntries(true);
    }

    private void thenRepositoryShouldRefresh() {
        verify(mockRepository).getFeedEntries();
    }

    private void thenViewShouldSetRefreshing() {
        verify(mockView).setRefreshing();
    }

    private void whenRefreshDataCalled() {
        feedListPresenter.refreshData();
    }

    private void givenViewIsNotRefreshing() {
        when(mockView.isRefreshing()).thenReturn(false);
    }

    private void thenViewShouldNotSetRefreshing() {
        verify(mockView, times(0)).setRefreshing();
    }

    private void givenViewIsRefreshing() {
        when(mockView.isRefreshing()).thenReturn(true);
    }

    private List<FeedEntry> getListOfFeedEntries(int count) {
        List<FeedEntry> feedEntries = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            feedEntries.add(new FeedEntry(null, null, "Text for entry: " + count, new Date(), Event.COMMENT));
        }
        return feedEntries;
    }

}