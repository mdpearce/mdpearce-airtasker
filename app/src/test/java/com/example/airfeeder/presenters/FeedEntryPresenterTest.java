package com.example.airfeeder.presenters;

import com.example.airfeeder.R;
import com.example.airfeeder.io.remote.ImageUrlFormatter;
import com.example.airfeeder.model.Event;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.model.Profile;
import com.example.airfeeder.model.Task;
import com.example.airfeeder.model.TaskState;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class FeedEntryPresenterTest {

    private FeedEntryPresenter feedEntryPresenter;

    @Mock
    private FeedEntryPresenter.View mockView;

    private FeedDateFormatter feedDateFormatter;

    private static final String TEST_NAME = "Michael";
    private static final String TEST_TASK_NAME = "Some task";

    private static final Profile TEST_PROFILE = new Profile(1, "/someurl.jpg", TEST_NAME, 4);
    private static final Task TEST_TASK = new Task(2, TEST_TASK_NAME, "Some description", TaskState.ASSIGNED, 1, 2);
    private static final FeedEntry TEST_FEED_ENTRY = new FeedEntry(TEST_TASK, TEST_PROFILE, "{profileName} asked a question about {taskName}", new Date(), Event.COMMENT);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        feedDateFormatter = new FeedDateFormatter(Locale.US);
        feedEntryPresenter = new FeedEntryPresenter(mockView, feedDateFormatter);
    }

    @Test
    public void shouldClearFieldsWhenNewDataSet() throws Exception {
        whenSetDataCalled();
        thenViewShouldClearDescription();
        thenViewShouldClearDate();
        thenViewShouldClearEvent();
        thenViewShouldSetAvatarToPlaceholder();
    }

    @Test
    public void shouldSetDescriptionTextCorrectly() throws Exception {
        whenSetDataCalled();
        thenViewShouldSetDescriptionTo(new DescriptionFormatter(TEST_PROFILE, TEST_TASK).formatDescription(TEST_FEED_ENTRY.getText()));
    }

    @Test
    public void shouldSetDateCorrectly() throws Exception {
        whenSetDataCalled();
        thenViewShouldSetDateTextTo(feedDateFormatter.formatDate(TEST_FEED_ENTRY.getCreatedAt()));
    }

    @Test
    public void shouldSetEventNameCorrectly() throws Exception {
        whenSetDataCalled();
        thenViewShouldSetEventNameTo(TEST_FEED_ENTRY.getEvent().name().toLowerCase(Locale.getDefault()));
    }

    @Test
    public void shouldSetAvatarUrlCorrectly() throws Exception {
        whenSetDataCalled();
        thenViewShouldSetAvatarToUrl(ImageUrlFormatter.formatAvatarMiniUrl(TEST_PROFILE.getAvatarMiniUrl()));
    }

    private void thenViewShouldSetAvatarToUrl(String url) {
        verify(mockView).setAvatarUrl(eq(url), eq(R.drawable.ic_person_black_96dp));
    }

    private void thenViewShouldSetEventNameTo(String eventName) {
        verify(mockView).setEventText(eventName);
    }

    private void thenViewShouldSetDateTextTo(String dateText) {
        verify(mockView).setDateText(dateText);
    }

    private void thenViewShouldSetDescriptionTo(String description) {
        verify(mockView).setDescriptionText(description);
    }

    private void thenViewShouldSetAvatarToPlaceholder() {
        verify(mockView).setAvatarDrawable(R.drawable.ic_person_black_96dp);
    }

    private void thenViewShouldClearEvent() {
        verify(mockView).setEventText("");
    }

    private void thenViewShouldClearDate() {
        verify(mockView).setDateText("");
    }

    private void thenViewShouldClearDescription() {
        verify(mockView).setDescriptionText("");
    }

    private void whenSetDataCalled() {
        feedEntryPresenter.setData(TEST_FEED_ENTRY);
    }

}