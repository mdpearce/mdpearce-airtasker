package com.example.airfeeder.io;

import com.example.airfeeder.io.remote.AirFeederService;
import com.example.airfeeder.model.Event;
import com.example.airfeeder.model.FeedEntry;
import com.example.airfeeder.model.RawFeedEntry;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AirFeederRepositoryTest {

    private AirFeederRepository repository;

    @Mock
    private AirFeederService mockRemoteService;

    @Mock
    private TaskRepository mockTaskRepository;

    @Mock
    private ProfileRepository mockProfileRepository;

    private List<FeedEntry> cachedFeedEntries;

    @Mock
    private Call<List<RawFeedEntry>> mockFeedEntriesCall;

    @Mock
    private AirFeederProvider.FeedUpdateListener mockFeedUpdateListener;

    @Captor
    private ArgumentCaptor<Callback<List<RawFeedEntry>>> remoteServiceCallbackCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(mockRemoteService.feedEntries()).thenReturn(mockFeedEntriesCall);

        cachedFeedEntries = new ArrayList<>();

        repository = new AirFeederRepository(mockRemoteService, mockProfileRepository, mockTaskRepository, cachedFeedEntries);
        repository.addFeedUpdateListener(mockFeedUpdateListener);
    }

    @Test
    public void shouldReturnCacheWhenNotDirty() throws Exception {
        givenLocalCacheHasEntryCount(3);
        whenFeedEntriesRequested();
        thenRemoteServiceShouldNotBeHit();
        thenUpdateListenerShouldReceiveLocalCache();
    }

    @Test
    public void shouldHitRemoteServiceWhenCacheDirty() throws Exception {
        givenLocalCacheIsEmpty();
        whenFeedEntriesRequested();
        thenRemoteServiceShouldBeHit();
    }

    @Test
    public void shouldHitRemoteServiceWhenRefreshForcedAndCacheNotDirty() throws Exception {
        givenLocalCacheHasEntryCount(3);
        whenFeedEntriesRequestedWithForceRefresh();
        thenRemoteServiceShouldBeHit();
    }

    @Test
    public void shouldCacheDataReturnedFromRemoteService() throws Exception {
        List<RawFeedEntry> rawFeedEntries = getListOfRawFeedEntries(3);
        givenLocalCacheIsEmpty();
        whenFeedEntriesRequested();
        whenRemoteServiceReturnsListOfRawFeedEntries(rawFeedEntries);
        thenLocalCacheShouldContainRawEntries(rawFeedEntries);
    }

    private void thenLocalCacheShouldContainRawEntries(List<RawFeedEntry> rawFeedEntries) {
        assertThat(cachedFeedEntries.size(), is(rawFeedEntries.size()));
        for (int i = 0; i < rawFeedEntries.size(); i++) {
            RawFeedEntry rawFeedEntry = rawFeedEntries.get(i);
            FeedEntry cachedFeedEntry = cachedFeedEntries.get(i);
            assertThat(cachedFeedEntry.getText(), is(rawFeedEntry.getText()));
            assertThat(cachedFeedEntry.getCreatedAt(), is(rawFeedEntry.getCreatedAt()));
            assertThat(cachedFeedEntry.getEvent(), is(rawFeedEntry.getEvent()));
        }
    }

    private void whenRemoteServiceReturnsListOfRawFeedEntries(List<RawFeedEntry> feedEntries) {
        verify(mockFeedEntriesCall).enqueue(remoteServiceCallbackCaptor.capture());
        Response<List<RawFeedEntry>> mockFeedEntriesResponse = Response.success(feedEntries);
        remoteServiceCallbackCaptor.getValue().onResponse(mockFeedEntriesCall, mockFeedEntriesResponse);
    }

    private void whenFeedEntriesRequestedWithForceRefresh() {
        repository.getFeedEntries(true);
    }

    private void thenRemoteServiceShouldBeHit() {
        verify(mockRemoteService, times(1)).feedEntries();
    }

    private void givenLocalCacheIsEmpty() {
        cachedFeedEntries.clear();
    }

    private void thenUpdateListenerShouldReceiveLocalCache() {
        verify(mockFeedUpdateListener).feedUpdated(eq(cachedFeedEntries));
    }

    private void thenRemoteServiceShouldNotBeHit() {
        verify(mockRemoteService, never()).feedEntries();
    }

    private void whenFeedEntriesRequested() {
        repository.getFeedEntries();
    }

    private void givenLocalCacheHasEntryCount(int count) {
        cachedFeedEntries.addAll(getListOfFeedEntries(count));
    }

    private List<FeedEntry> getListOfFeedEntries(int count) {
        List<FeedEntry> feedEntries = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            feedEntries.add(new FeedEntry(null, null, "Text for entry: " + count, new Date(), Event.COMMENT));
        }
        return feedEntries;
    }

    private List<RawFeedEntry> getListOfRawFeedEntries(int count) {
        List<RawFeedEntry> feedEntries = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            feedEntries.add(new RawFeedEntry(1, 1, "Text for entry: " + count, new Date(), Event.COMMENT));
        }
        return feedEntries;
    }

}