package com.example.airfeeder.io.remote;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ImageUrlFormatterTest {

    @Test
    public void shouldFormatUrlCorrectly() throws Exception {
        assertEquals("https://stage.airtasker.com/android-code-test/img/1.jpg",
                ImageUrlFormatter.formatAvatarMiniUrl("img/1.jpg"));
        assertEquals("https://stage.airtasker.com/android-code-test/img/2.png",
                ImageUrlFormatter.formatAvatarMiniUrl("img/2.png"));
        assertEquals("https://stage.airtasker.com/android-code-test//img/3.jpg",
                ImageUrlFormatter.formatAvatarMiniUrl("/img/3.jpg"));
    }

    @Test
    public void shouldReturnNullWhenUrlEmpty() throws Exception {
        assertNull(ImageUrlFormatter.formatAvatarMiniUrl(""));
    }

    @Test
    public void shouldReturnNullWhenUrlNull() throws Exception {
        assertNull(ImageUrlFormatter.formatAvatarMiniUrl(null));
    }

}