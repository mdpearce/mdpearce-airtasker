# README #

AirTasker Android tech challenge

Michael Pearce
mdpearce@gmail.com

### Overview ###

* Implementation of a feed reader to match design specification
* Built with Android Studio 3.0-rc1, if there are any issues getting it up and running with previous versions, let me know and I can provide a version for 2.3

### Future enhancement ideas ###

* Increase test coverage
* Add Espresso-based integration tests
* Flesh out repositories. Currently they use a rudimentary memory-based cache only. In the future, a disk-backed cache could be implemented, using SQLite or another solution
* Enhance visual design. Priority was to match spec at this stage, but there is room for improvement vs Google's Material Design guidelines
* Enhance configuration change experience. Currently, network requests will not be repeated on rotation so the experience is largely instant, but scroll position is not restored.
* Think about using a stream-based solution such as RxJava for data access layer. This may help to simplify the logic of fetching data from different endpoints
